var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require("path");

app.get('/', function(req, res){
    io.emit('log', 'A new connection has opened');
    console.log("Hello");
    res.send("A new connection");
});

app.get('/log', function(req, res){
	res.sendFile(__dirname + '/log.html');
})

app.use("/static", express.static(__dirname + '/public'));

http.listen(3000, function(){
  console.log('listening on port: 3000');
});
